package test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import main.AnnualException;
import main.Loancalc;

import org.junit.Test;

public class TestLoancalc {

	@Test
	public void annual() {
		Loancalc lc = new Loancalc();
		lc.setAnnual(12);
		assertThat(lc.getAnnual(),is(12));
		lc.setAnnual(18);
		assertThat(lc.getAnnual(), is(18));
	}

	@Test(expected = AnnualException.class)
	public void annualerror() {
		Loancalc lc = new Loancalc();
		lc.setAnnual(-1);
	}

	@Test
	public void testCalcInterest() {
		Loancalc lc = new Loancalc();
		lc.setBalance(100000);
		lc.setAnnual(12);
		assertThat(lc.calcInterest(), is(1000));
	}
    @Test
    public void testPayback(){
    Loancalc lc=new Loancalc();
    lc.setBalance(100000);
    lc.setAnnual(12);
    lc.payback(10000+lc.calcInterest());
    assertThat(lc.payback(),90000);
    }
}